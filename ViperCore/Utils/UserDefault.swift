import Foundation

@propertyWrapper
public struct UserDefault<T> {
    let key: String
    let defaultValue: T

    public init(_ key: String, defaultValue: T) {
        self.key = key
        self.defaultValue = defaultValue
    }

    public var wrappedValue: T {
        get {
            guard let value = UserDefaults.standard.object(forKey: key) else {
                return defaultValue
            }
            return value as? T ?? defaultValue
        }
        set {
            if let value = newValue as? OptionalProtocol, value.isNil() {
                UserDefaults.standard.removeObject(forKey: key)
            } else {
                UserDefaults.standard.set(newValue, forKey: key)
            }
        }
    }
}

public protocol OptionalProtocol {
    func isNil() -> Bool
}

extension Optional: OptionalProtocol {
    public func isNil() -> Bool {
        return self == nil
    }
}
