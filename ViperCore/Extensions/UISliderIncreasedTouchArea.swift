import CoreGraphics
import UIKit

public final class UISliderIncreasedTouchArea: UISlider {
    var touchableArea = CGSize(width: 20, height: 20)

    public override func point(inside point: CGPoint, with _: UIEvent?) -> Bool {
        var bounds: CGRect = self.bounds
        bounds = bounds.insetBy(dx: -touchableArea.width, dy: -touchableArea.height)
        return bounds.contains(point)
    }
}
