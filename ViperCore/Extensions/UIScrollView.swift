import UIKit

public extension UIScrollView {
    
    func setRefreshIndicator(height: CGFloat, text: String, textColor: UIColor, backgroundColor: UIColor) -> UIRefreshControl {
        let refresh = UIRefreshControl(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: height))
        
        let refreshTitle = NSMutableAttributedString(string: text)
        refreshTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: NSMakeRange(0, refreshTitle.length))
        
        refresh.attributedTitle = refreshTitle
        refresh.backgroundColor = backgroundColor
        
        if #available(iOS 10.0, *) {
            self.refreshControl = refresh
        } else {
            self.addSubview(refresh)
        }
        
        return refresh
    }
    
    func animateBottomView(_ view: UIView, visibleWhenEmpty: Bool = false, animated: Bool = true) {
        guard contentSize.height > 0 else {
            animate(view, to: visibleWhenEmpty ? 1 : 0, animated: animated)
            return
        }
        let bottomEdge = contentOffset.y + bounds.height - contentInset.bottom
        if bottomEdge >= contentSize.height {
            // Reached bottom
            animate(view, to: 1)
        }
        guard contentSize.height + contentInset.bottom >= bounds.height else {
            animate(view, to: 1)
            return
        }
        if panGestureRecognizer.translation(in: self).y > 0 && view.alpha > 0 {
            // Scrolling up
            animate(view, to: 0)
        }
    }
    
    func forceVisibility(view: UIView) {
        animate(view, to: 1)
    }
    
    private func animate(_ view: UIView, to alpha: CGFloat, animated: Bool = true) {
        UIView.animate(withDuration: animated ? 0.2 : 0.0) {
            view.alpha = alpha
        }
    }
}
