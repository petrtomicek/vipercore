import Foundation

public extension String {
    
    var doubleString: String {
        return self.replacingOccurrences(of: ",", with: ".")
    }
    
    var doubleValue: Double? {
        return Double(doubleString)
    }
    
    var doubleOrZeroValue: Double {
        return Double(doubleString) ?? 0
    }
    
    func notContains(_ value: String?) -> Bool {
        guard let value = value else { return false }
        return !contains(value)
    }
    
    var capitalizingFirstLetter: String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter
    }
    
    var withRegularCharacters: String {
        return String(unicodeScalars.filter({ $0.utf8.count == 1 }))
        //let set = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890- ._+")
        //return String(unicodeScalars.filter(set.contains))
    }
}

public extension Optional where Wrapped == String {
    
    var doubleString: String {
        return self?.replacingOccurrences(of: ",", with: ".") ?? ""
    }
    
    var doubleValue: Double? {
        return Double(doubleString)
    }
    
    var doubleOrZeroValue: Double {
        return Double(doubleString) ?? 0
    }
}
