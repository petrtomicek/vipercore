import UIKit

extension UICollectionView {
    public var scrolledDown: Bool {
        return self.contentOffset.y > 0
    }
    
    public var visibleCellIndex: IndexPath? {
        // MARK: Edge cases
        guard self.indexPathsForVisibleItems.count > 0 else { return nil }
        guard self.indexPathsForVisibleItems.count > 1 else {
            return self.indexPathsForVisibleItems.first
        }
        
        let midX = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        let cellsDistances = self.visibleCells.map { (centerDistance: hypot(midX.x - $0.frame.midX, midX.y - $0.frame.midY), cell: $0) }
        
        guard let closestCell = cellsDistances.min(by: { $0.centerDistance < $1.centerDistance })?.cell else {
            return self.indexPathsForVisibleItems.first
        }
        
        return self.indexPath(for: closestCell)
    }
    
    public func scrollTo(row: Int, section: Int = 0, at position: UICollectionView.ScrollPosition = .centeredHorizontally, animated: Bool) {
        guard section < self.numberOfSections, row < self.numberOfItems(inSection: section) else { return }
        
        let selectedIndexPath = IndexPath(row: row, section: section)
        self.scrollToItem(at: selectedIndexPath, at: position, animated: animated)
    }
    
    public func scrollToFirst(at position: UICollectionView.ScrollPosition, animated: Bool) {
        self.scrollTo(row: 0, section: 0, at: position, animated: animated)
    }
    
    public func reloadSelectedItems() {
        guard let selectedItemsIndexes = self.indexPathsForSelectedItems else { return }
        
        UIView.performWithoutAnimation {
            self.reloadItems(at: selectedItemsIndexes)
        }
        
        for indexPath in selectedItemsIndexes {
            self.deselectItem(at: indexPath, animated: false)
        }
    }
}
