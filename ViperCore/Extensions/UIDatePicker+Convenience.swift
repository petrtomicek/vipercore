import UIKit

public extension UIDatePicker {
    static func birthdayPicker(minAge: Int, selectedAge: Int) -> UIDatePicker {
        let datePickerView: UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        var dateComponent = DateComponents()
        dateComponent.year = -minAge
        
        // MARK: Set max available date
        datePickerView.maximumDate = Calendar.current.date(byAdding: dateComponent, to: Date())
        
        dateComponent.year = -selectedAge
        // MARK: Set a selected date
        if let selectedDate = Calendar.current.date(byAdding: dateComponent, to: Date()) {
            datePickerView.setDate(selectedDate, animated: true)
        }
        return datePickerView
    }
}
