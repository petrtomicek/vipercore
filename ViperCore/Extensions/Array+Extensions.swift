import Foundation

public extension Array {
    
    var nilIfEmpty: Array? {
        return count == 0 ? nil : self
    }

    func chunked(by chunkSize: Int) -> [[Element]] {
        return stride(from: 0, to: self.count, by: chunkSize).map {
            Array(self[$0..<Swift.min($0 + chunkSize, self.count)])
        }
    }
    
    func take(_ length: Int) -> [Element] {
        if length >= count { return self }
        return (0..<length).map { self[$0] }
    }
    
    func skip(_ length: Int) -> [Element] {
        if length >= count { return [] }
        return ((length - 1)..<count).map { self[$0] }
    }
}

public extension Array where Element: Equatable {
    
    @inlinable func notContains(_ element: Element) -> Bool {
        return !self.contains(element)
    }
    
    @inlinable func containsAny(of elements: [Element]) -> Bool {
        for element in elements {
            if self.contains(element) { return true }
        }
        return false
    }
}

public extension Array where Element: Hashable {
    
    var distinct: [Element] {
        return Array(Set(self))
    }
    
    var distinctedOrdered: [Element] {
        var tempArray: [Element] = []
        for item in self {
            if tempArray.contains(item) {
                continue
            } else {
                tempArray.append(item)
            }
        }
        return tempArray
    }
}
