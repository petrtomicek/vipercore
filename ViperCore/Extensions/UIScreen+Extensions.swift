import UIKit

public extension UIScreen {
    
    static var isSmall: Bool {
        return UIScreen.main.bounds.height == 568
    }
    
    static var isXStyle: Bool {
        return UIScreen.main.bounds.height > 736
    }
    
    var width: CGFloat {
        return UIScreen.main.bounds.width
    }
}
