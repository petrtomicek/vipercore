import Foundation

public extension NSAttributedString {
    
    var isEmpty: Bool {
        return string.isEmpty
    }

    // Concatenate attributed strings
    static func + (left: NSAttributedString, right: NSAttributedString) -> NSAttributedString {
        let result = NSMutableAttributedString()
        result.append(left)
        result.append(right)
        return result
    }
}
