import UIKit

public extension UITableView {
    
    var scrolledDown: Bool {
        return self.contentOffset.y > 0
    }
    
    /**
     Scroll top top in UITableView.
     
     - parameter animated:
     
     By default, the animation is set to true.
     **/
    func scrollToTop(animated: Bool = true) {
        setContentOffset(CGPoint.zero, animated: animated)
    }
    
    func scrollToBottom(animated: Bool = true, position: ScrollPosition) {
        let lastSectionIndex = max(0, numberOfSections - 1)
        let rowCount = numberOfRows(inSection: lastSectionIndex)
        guard rowCount > 0 else { return }
        scrollToRow(at: IndexPath(row: rowCount - 1, section: lastSectionIndex), at: position, animated: animated)
    }
    
    func scrollToFirst(at position: UITableView.ScrollPosition, animated: Bool) {
        guard self.numberOfSections > 0, self.numberOfRows(inSection: 0) > 0 else {
            self.scrollToTop(animated: animated)
            return
        }
        self.scrollToRow(at: IndexPath(row: 0, section: 0), at: position, animated: animated)
    }
    
    /**
     Dequeues a UITableViewCell for use in a UITableView.
     
     - parameter type: The type of the cell.
     - parameter reuseIdentifier: The reuse identifier for the cell (optional).
     
     - returns: A force-casted UITableViewCell of the specified type.
     
     By default, the class name of the cell is used as the reuse identifier.
     
     Example:
     ```
     let cell = tableView.dequeueReusableCell(CustomCell)
     ```
     */
    func dequeueCell<T: UITableViewCell>(_ type: T.Type = T.self, withIdentifier reuseIdentifier: String = String(describing: T.self)) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: reuseIdentifier) as? T else {
            fatalError("Unknown cell type (\(T.self)) for reuse identifier: \(reuseIdentifier)")
        }
        return cell
    }
    
    /**
     Register a UITableViewCell for use in a UITableView.
     
     - parameter type: The type of cell to register.
     - parameter reuseIdentifier: The reuse identifier for the cell (Optional)
     
     By default, the class name of the cell is used as the reuse identifier.
     **/
    func registerCell<T: UITableViewCell>(_ type: T.Type, withIdentifier reuseIdentifier: String = String(describing: T.self)) {
        register(type, forCellReuseIdentifier: reuseIdentifier)
    }
    
    /**
     Register a UITableViewHeaderFooter for use in a UITableView.
     
     - parameter type: The type of headerFooterView to register
     - parameter reuseIdentifier: The reuse identifier for the view (Optional)
     
     By default, the class name of the view is used as the reuse identifier.
     **/
    func registerHeaderFooter<T: UITableViewHeaderFooterView>(_ type: T.Type, withIdentifier reuseIdentifier: String = String(describing: T.self)) {
        register(type, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
    }
    
    func reloadSelectedRows(with animation: UITableView.RowAnimation) {
        guard let selectedRowsIndexes = self.indexPathsForSelectedRows else {
            return
        }
        
        self.reloadRows(at: selectedRowsIndexes, with: animation)
        
        for indexPath in self.indexPathsForSelectedRows ?? [] {
            self.deselectRow(at: indexPath, animated: false)
        }
    }
}
