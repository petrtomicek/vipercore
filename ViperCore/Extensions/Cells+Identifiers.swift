import Foundation
import UIKit

public extension NSObject {

    static var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    var className: String {
        return type(of: self).className
    }
}

public extension UITableViewCell {

    static var reuseIdentifier: String {
        return self.className
    }

    static func registerClass(for tableView: UITableView) {
        tableView.register(self, forCellReuseIdentifier: reuseIdentifier)
    }

    var tableView: UITableView? {
        return superview(type: UITableView.self)
    }
}

public extension UICollectionReusableView {

    static var reuseIdentifier: String {
        return self.className
    }

    static func registerClass(for collectionView: UICollectionView) {
        collectionView.register(self, forCellWithReuseIdentifier: reuseIdentifier)
    }
}

public extension UICollectionViewCell {

    static var identifier: String {
        return self.className
    }

    static func register(_ collectionView: UICollectionView) {
        collectionView.register(self, forCellWithReuseIdentifier: identifier)
    }
}
