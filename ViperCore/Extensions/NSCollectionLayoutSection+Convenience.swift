import UIKit

public extension UIViewController {
    
    static func fullWidthDynamicHeightSection(height: NSCollectionLayoutDimension = .estimated(100)) -> NSCollectionLayoutSection {
        let layoutSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: height
        )
        let item = NSCollectionLayoutItem(layoutSize: layoutSize)
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: layoutSize, subitem: item, count: 1)
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = .zero
        return section
    }

    static func absoluteWidthDynamicHeightSection(width: CGFloat, height: NSCollectionLayoutDimension = .estimated(100)) -> NSCollectionLayoutSection {
        let layoutSize = NSCollectionLayoutSize(
            widthDimension: .absolute(width),
            heightDimension: height
        )
        let item = NSCollectionLayoutItem(layoutSize: layoutSize)
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: layoutSize, subitem: item, count: 1)
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuous
        section.interGroupSpacing = 8
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)
        return section
    }

    static func twoItemsDynamicHeight(frameWidth: CGFloat) -> NSCollectionLayoutSection {
        let numberOfItemsInOneRow = 2
        let leadingTrailingInset: CGFloat = 16
        let interItemSpacing: CGFloat = 16
        let heightConstant = (frameWidth
            - (CGFloat(numberOfItemsInOneRow - 1) * interItemSpacing)
            - 2 * leadingTrailingInset)
            / CGFloat(numberOfItemsInOneRow)

        let itemHeight: NSCollectionLayoutDimension = .absolute(heightConstant)
        let itemLayoutSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: itemHeight
        )
        let item = NSCollectionLayoutItem(layoutSize: itemLayoutSize)

        let groupLayoutSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: itemHeight
        )
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupLayoutSize, subitem: item, count: numberOfItemsInOneRow)
        group.interItemSpacing = .fixed(interItemSpacing)

        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: leadingTrailingInset, bottom: 0, trailing: leadingTrailingInset)
        section.interGroupSpacing = 16
        return section
    }
}
