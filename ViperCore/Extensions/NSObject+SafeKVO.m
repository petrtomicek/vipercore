#import "NSObject+SafeKVO.h"

@implementation NSObject (SafeKVO)

- (BOOL)tryRemoveObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath
{
    
    BOOL result = YES;
    
    @try {
        [self removeObserver:observer forKeyPath:keyPath];
    } @catch (NSException *exception) {
        result = NO;
    }
    
    return result;
}

@end
