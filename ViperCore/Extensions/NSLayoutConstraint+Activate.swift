import Foundation
import UIKit

extension NSLayoutConstraint {
    
    func activate() {
        self.isActive = true
    }
    
    func deactivate() {
        self.isActive = false
    }
}

extension Array where Element == NSLayoutConstraint {
    
    func activate() {
        forEach({ $0.activate() })
    }
}
