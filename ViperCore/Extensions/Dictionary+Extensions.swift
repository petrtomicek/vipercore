import Foundation

extension Dictionary {
    
    public var json: Data? {
        guard JSONSerialization.isValidJSONObject(self) else {
            return nil
        }
        return try? JSONSerialization.data(withJSONObject: self)
    }

    public func map<T: Hashable, U>(transform: (Key, Value) throws -> (T, U)) rethrows -> [T: U] {
        var result: [T: U] = [:]
        for (key, value) in self {
            let (transformedKey, transformedValue) = try transform(key, value)
            guard result[transformedKey] == nil else {
                fatalError("Two keys from the original dictionary mapped to the same value. This is disallowed as it leads to non-deterministic and likely unwanted behavior.")
            }
            result[transformedKey] = transformedValue
        }
        return result
    }
}

