import UIKit

public extension UITableView {
    
    func layoutTableHeaderView() {
        guard let headerView = self.tableHeaderView else { return }
        headerView.translatesAutoresizingMaskIntoConstraints = false
        let headerWidth = headerView.bounds.size.width
        let temporaryWidthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "[headerView(width)]",
                                                                       options: .alignAllLeft,
                                                                       metrics: ["width": headerWidth],
                                                                       views: ["headerView": headerView])
        headerView.addConstraints(temporaryWidthConstraints)
        headerView.layoutIfNeeded()
        let headerSize = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        let height = headerSize.height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        tableHeaderView = headerView
        headerView.removeConstraints(temporaryWidthConstraints)
        headerView.translatesAutoresizingMaskIntoConstraints = true
    }

    func layoutTableFooterView() {
        guard let footerView = self.tableFooterView else { return }
        footerView.translatesAutoresizingMaskIntoConstraints = false
        let footerWidth = footerView.bounds.size.width
        let temporaryWidthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "[footerView(width)]",
                                                                       options: .alignAllLeft,
                                                                       metrics: ["width": footerWidth],
                                                                       views: ["footerView": footerView])
        footerView.addConstraints(temporaryWidthConstraints)
        footerView.layoutIfNeeded()
        let footerSize = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        let height = footerSize.height
        var frame = footerView.frame
        frame.size.height = height
        footerView.frame = frame
        tableFooterView = footerView
        footerView.removeConstraints(temporaryWidthConstraints)
        footerView.translatesAutoresizingMaskIntoConstraints = true
    }
}
