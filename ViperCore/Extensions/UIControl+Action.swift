import UIKit

public typealias UIControlTargetAction = (UIControl) -> Void

public class ActionWrapper: NSObject {

    let closure: UIControlTargetAction
    init(_ closure: @escaping UIControlTargetAction) {
        self.closure = closure
    }
}

public extension UIControl {
    
    private struct AssociatedKeys {
        static var targetClosure = "targetClosure"
    }
    
    private var targetClosure: UIControlTargetAction? {
        get {
            guard let closureWrapper = objc_getAssociatedObject(self, &AssociatedKeys.targetClosure) as? ActionWrapper else { return nil }
            return closureWrapper.closure
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.targetClosure, ActionWrapper(newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func addAction(for event: UIControl.Event = .touchUpInside, _ closure: @escaping UIControlTargetAction) {
        targetClosure = closure
        addTarget(self, action: #selector(UIControl.closureAction), for: event)
    }
    
    @objc func closureAction() {
        guard let targetClosure = targetClosure else { return }
        targetClosure(self)
    }
}
