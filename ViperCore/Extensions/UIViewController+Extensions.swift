import UIKit

public extension UIViewController {
    
    var isVisible: Bool {
        if isViewLoaded {
            return view.window != nil
        }
        return false
    }

    var isTopViewController: Bool {
        if navigationController != nil {
            return navigationController?.visibleViewController === self
        } else if tabBarController != nil {
            return tabBarController?.selectedViewController == self && presentedViewController == nil
        } else {
            return presentedViewController == nil && isVisible
        }
    }

    var isCompactSizeClass: Bool {
        return traitCollection.verticalSizeClass == .compact
    }
}

public extension UIViewController {
    
    func configureKeyboardDismissOnTap(cancelsTouchesInView: Bool = false) {
        let dismissKeyboardTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        dismissKeyboardTap.cancelsTouchesInView = cancelsTouchesInView
        view.addGestureRecognizer(dismissKeyboardTap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

public extension UINavigationController {
    
    func removeViewController(_ controller: UIViewController.Type) {
        if let viewController = viewControllers.first(where: { $0.isKind(of: controller.self) }) {
            viewController.removeFromParent()
        }
    }
}
