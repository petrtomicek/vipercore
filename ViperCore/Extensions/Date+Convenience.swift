import Foundation
import DateToolsSwift

public extension Date {
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon) ?? self
    }

    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon) ?? self
    }

    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self) ?? self
    }

    var monthValue: Int {
        return Calendar.current.component(.month, from: self)
    }

    var isLastDayOfMonth: Bool {
        return tomorrow.monthValue != monthValue
    }

    func timeAgoDisplay() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = self < Date().adding(minutes: -60) ? .full : .short
        return formatter.localizedString(for: self, relativeTo: Date())
    }
    
    func differenceInDays(withDate date: Date) -> Int {
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: date)
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        return components.day ?? 0
    }

    func adding(seconds: Int) -> Date {
        return Calendar.current.date(byAdding: .second, value: seconds, to: self)!
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func adding(hours: Int) -> Date {
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)!
    }
    
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    func adding(months: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
    
    func adding(years: Int) -> Date {
        return Calendar.current.date(byAdding: .year, value: years, to: self)!
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var onlyDate: Date {
        get {
            let calendar = Calendar.current
            var dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
            dateComponents.timeZone = NSTimeZone.system
            return calendar.date(from: dateComponents) ?? self
        }
    }
    
    var startOfWeek: Date {
        let calendar = Calendar.current
        return calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    
    var endOfWeek: Date {
        let startOfWeek = self.startOfWeek
        return Calendar.current.date(byAdding: .day, value: 7, to: startOfWeek)!
    }
    
    var isCurrentWeek: Bool {
        let currentDate = Date()
        return self.isLaterThanOrEqual(to: currentDate.startOfWeek) && self.isEarlier(than: currentDate.endOfWeek)
    }
    
    var startOfMonth: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    var endOfMonth: Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth)!
    }
    
    func convertToString(dateformat formatType: DateFormatType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatType.rawValue
        let newDate: String = dateFormatter.string(from: self)
        return formatType == .time ? newDate.lowercased() : newDate
    }
    
    func convertToString(stringFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = stringFormat
        return dateFormatter.string(from: self)
    }
    
    func timeOnly(hour: Int, minutes: Int = 0) -> Date {
        return Calendar.current.date(bySettingHour: hour, minute: minutes, second: 0, of: self)!
    }
    
    static func time(bySettingHour: Int, minutes: Int = 0) -> Date {
        return Calendar.current.date(bySettingHour: bySettingHour, minute: minutes, second: 0, of: Date())!
    }
}

public extension DateFormatter {
    
    func strongString(for date: Date?) -> String {
        guard let date = date else { return "" }
        return string(from: date)
    }
    
    func optionalString(for date: Date?) -> String? {
        guard let date = date else { return nil }
        return string(from: date)
    }
}

public enum DateFormatType: String {
    /// 4:00 PM
    case time = "h:mm a"
    /// 4:00
    case shortTime = "h:mm"
    /// am/om
    case timeSymbol = "a"
    /// Monday 12/10
    case date = "EEEE MM/dd"
    /// 2021-01-14T18:00+00:00
    case serverDate = "yyyy-MM-dd'T'HH:mm:ssZ"
    /// Feb 4
    case shortMonthDay = "MMM d"
    /// Jul 20, 2021
    case shorthMonthDayYear = "MMM d, yyyy"
    /// Feb
    case shortMonth = "MMM"
    /// February
    case fullMonth = "MMMM"
    /// 20
    case shortDay = "d"
}
