import UIKit

public class PagingCollectionFlowLayout: UICollectionViewFlowLayout {
    
    override public init() {
        super.init()
        scrollDirection = .horizontal
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        scrollDirection = .horizontal
    }

    public func setup(collectionViewFrame: CGRect) {
        minimumLineSpacing = 0
        sectionInset = .zero
        itemSize = CGSize(width: collectionViewFrame.width, height: collectionViewFrame.height)
    }
}
