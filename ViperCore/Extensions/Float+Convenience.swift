import Foundation

public extension Float {
    
    func secondsToString() -> String {
        let seconds = String(format: "%02d", Int(self) % 60)
        let minutes = String(format: "%02d", Int(self) / 60)
        let hours = String(format: "%02d", Int(self) / 3600)
        return hours == "00" ? "\(minutes):\(seconds)" : "\(hours):\(minutes):\(seconds)"
    }
}
