import UIKit

extension UIDevice {
    
    public static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }

    public static var isTablet: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    public static var isPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.size.height
    }
}
