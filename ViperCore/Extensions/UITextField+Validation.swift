import UIKit

private let ValidationClosureKey = "ValidationClosureKey"
private let ValidationClosureRestKey = "ValidationClosureRestKey"
private let StateChangedClosureKey = "StateChangedClosureKey"
private let LastResultKey = "LastResultKey"

public typealias TFValidationClosure = (String?) -> Bool
public typealias TFStateChangedClosure = (UITextField, Bool) -> ()

public enum TFValidationType {
    case required
    case email
    case integer
    case double
    case minLength(Int)
    case maxLength(Int)
    case length(Int, Int)
    case alphanumeric
    case phoneNumber(Int)
    case custom(TFValidationClosure)
}

private let requiredValidationClosure: TFValidationClosure = { $0 != nil && !$0!.isEmpty }

private let emailValidationClosure: TFValidationClosure = { (text) in
    if text != nil {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: text!)
    }
    return false
}

private let integerValidationClosure: TFValidationClosure = { (text) in
    if text != nil {
        if let number = Int(text!) {
            return true
        }
    }
    return false
}

private let doubleValidationClosure: TFValidationClosure = { (text) in
    if text != nil {
        if let number = Double(text!) {
            return true
        }
    }
    return false
}

private func minLengthValidationClosure(_ length: Int) -> TFValidationClosure {
    return { $0 != nil && $0!.count >= length }
}

private func maxLengthValidationClosure(_ length: Int) -> TFValidationClosure {
    return { $0 != nil && $0!.count > 0 && $0!.count <= length }
}

private func lengthValidationClosure(minLength: Int, maxLength: Int) -> TFValidationClosure {
    return { $0 != nil && $0!.count >= minLength && $0!.count <= maxLength }
}

private let alphanumericValidationClosure: TFValidationClosure =  { (text) in
    if text != nil && text!.count > 0 {
        return text!.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil
    }
    return false
}

private func phoneNumberValidationClosure(_ phoneNumberLenth: Int) -> TFValidationClosure {
    return {
        guard let unformattedNumber = $0?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {
            return false
        }
        return unformattedNumber.count == phoneNumberLenth && CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: unformattedNumber))
    }
}


// MARK: - UITextField extension

public func validate(textFields: [UITextField]) -> (invalid: [UITextField], compoundResult: Bool) {
    var invalidTextFields = [UITextField]()
    textFields.forEach { (textField) in
        if !textField.validate() {
            invalidTextFields.append(textField)
        }
    }
    return (invalidTextFields, invalidTextFields.count == 0)
}

public extension UITextField {
    
    var validationClosure: TFValidationClosure? {
        get { return getAssociatedValue(key: ValidationClosureKey, object: self) }
        set(newValue) { setAssociatedValue(associatedValue: newValue, key: ValidationClosureKey, object: self) }
    }
    
    var validationClosureRest: [TFValidationClosure]? {
        get { return getAssociatedValue(key: ValidationClosureRestKey, object: self) }
        set(newValue) { setAssociatedValue(associatedValue: newValue, key: ValidationClosureRestKey, object: self) }
    }
    
    var stateChangedClosure: TFStateChangedClosure? {
        get { return getAssociatedValue(key: StateChangedClosureKey, object: self) }
        set(newValue) { setAssociatedValue(associatedValue: newValue, key: StateChangedClosureKey, object: self) }
    }
    
    private(set) var lastResult: Bool? {
        get { return getAssociatedValue(key: LastResultKey, object: self) }
        set(newValue) { setAssociatedValue(associatedValue: newValue, key: LastResultKey, object: self) }
    }
    
    func setValidation(_ validation: TFValidationType, andValidation: TFValidationType) {
        setValidation(validation, rest: [andValidation])
    }
    
    func setValidation(_ validation: TFValidationType, rest: [TFValidationType]? = nil) {
        validationClosure = closure(forType: validation)
        if let theRest = rest {
            validationClosureRest = theRest.map { closure(forType: $0)! }
        }
    }
    
    func validate() -> Bool {
        let firstResult = validationClosure?(self.text) ?? false
        lastResult = validationClosureRest?.reduce(firstResult) {
            $0 && $1(self.text) }
            ?? firstResult
        
        stateChangedClosure?(self, lastResult ?? true)
        return lastResult ?? true
    }
    
    func isValid() -> Bool {
        if lastResult == nil {
            return validate()
        }
        return lastResult ?? true
    }


    private func closure(forType: TFValidationType?) -> TFValidationClosure? {
        guard let type = forType else { return nil }
        
        switch type {
            case .required: return requiredValidationClosure
            case .email: return emailValidationClosure
            case .integer: return integerValidationClosure
            case .double: return doubleValidationClosure
            case .minLength(let length): return minLengthValidationClosure(length)
            case .maxLength(let length): return maxLengthValidationClosure(length)
            case .length(let minLength, let maxLength): return lengthValidationClosure(minLength: minLength, maxLength: maxLength)
            case .alphanumeric: return alphanumericValidationClosure
            case .phoneNumber(let phoneNumberLenth): return phoneNumberValidationClosure(phoneNumberLenth)
            case .custom(let closure): return closure
        }
    }
}


