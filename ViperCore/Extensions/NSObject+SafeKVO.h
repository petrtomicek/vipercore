#import <Foundation/Foundation.h>

@interface NSObject (SafeKVO)
- (BOOL)tryRemoveObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath;
@end
