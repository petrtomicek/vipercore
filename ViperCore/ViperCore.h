//
//  ViperCore.h
//  ViperCore
//
//  Created by Petr Tomicek on 04.01.2023.
//

#import <Foundation/Foundation.h>

//! Project version number for ViperCore.
FOUNDATION_EXPORT double ViperCoreVersionNumber;

//! Project version string for ViperCore.
FOUNDATION_EXPORT const unsigned char ViperCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ViperCore/PublicHeader.h>


