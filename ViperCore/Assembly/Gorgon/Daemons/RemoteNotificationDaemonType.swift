/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Gorgon
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Gorgon/blob/master/LICENSE
 *
 **************************************************************************************************/

import UIKit


// Pushes from legacy systems may not have a category.  We support them with this constant.
// Only one daemon can have the category set to this.
public let kRemoteNotificationNoCategory = "no.category"

public enum RemoteNotificationSource {
    case voip
    case interactive
}

/**
 This protocol can be used to register for remote notifications sent to the app via APNS
 */
public protocol RemoteNotificationDaemonType: DaemonType {
    
    /**
     Represents the APN category. This is must match what the server sends in the `category` property.
     This must be unique for each remote notification (e.g. 1 remote notification daemon per category)
     */
    var category: String { get }
    
    /**
     Handler for the remote notification or notification response. The app can be in the background or foreground when this
     notification is handled. You can check the application state using UIApplication.shared.applicationState.
     
     - parameter notification: the `aps` userInfo object
     - parameter source:       the original receiver of remote notification
     - parameter response:     (optional) the response from user interacting with a notification
     - parameter completion:   the completion handler to be called once the processing is completed

     Swift protocols do not allow optional parameters with defaults in function definitions, so we have two almost-identical
     function definitions with a default implementation in an extension for the one least likely to be used that calls the other.
     */
    func handleNotification(_ notification: [String: AnyObject], source: RemoteNotificationSource, completion: @escaping (UIBackgroundFetchResult) -> Void)
    func handleNotification(_ notification: [String: AnyObject], source: RemoteNotificationSource, response: UNNotificationResponse?, completion: @escaping (UIBackgroundFetchResult) -> Void)

}

public extension RemoteNotificationDaemonType {
    func handleNotification(_ notification: [String: AnyObject], source: RemoteNotificationSource, response: UNNotificationResponse?=nil, completion: @escaping (UIBackgroundFetchResult) -> Void) {
        handleNotification(notification, source: source, completion: completion)
    }
}


public protocol RemoteNotificationErrorDaemonType: DaemonType {
    
    /**
     Handle invalidate push token delegate method.
    */
    func didInvalidatePushToken()
}
