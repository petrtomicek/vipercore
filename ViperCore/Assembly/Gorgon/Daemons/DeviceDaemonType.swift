/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Gorgon
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Gorgon/blob/master/LICENSE
 *
 **************************************************************************************************/

import Foundation


/**
 The protocol for the DeviceDaemonType allows for device registration for tokens.
 */
public protocol DeviceDaemonType: DaemonType {
    
    /**
     The application has registered the device token that can be used to send push notifications
     
     - parameter token: the device token to target push notifications
     */
    func deviceTokenRegistered(_ token: String)
}
