/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Gorgon
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Gorgon/blob/master/LICENSE
 *
 **************************************************************************************************/

/**
 The DaemonType is an empty protocol that all daemons will extend from. This allows us to register a daemon
 1 time and let the app figure out its specific type when a certain event happens.
*/
public protocol DaemonType: AnyObject {

}
