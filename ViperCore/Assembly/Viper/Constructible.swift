/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Cobra
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Cobra/blob/master/LICENSE
 *
 **************************************************************************************************/

/**
 The Constructible protocol ensures that an implementation can be intialized through
 the default constructor. This allows us to lazy load modules and instantiate
 assemblies from property file names.
 */

public protocol Constructible {
    init()
}
