import Swinject

/**
 The Config object is used to bootstrap a VIPER App with assemblies and properties for the DI container
*/
public final class AppConfig {
    
    /// list of components to load into the container
    fileprivate let components: [ComponentType]
    
    /// list of properties from frameworks (e.g. properties.json)
    fileprivate let properties: [PropertyType]

    /**
     Will create a configuration object that can be used to bootstrap a VIPER application
     
     - parameter components:   the list of property keys that point to AssemblyType classes
     - parameter properties:   a list of property files that will be loaded into the assembler for property resolution
     */
    public init(components: [ComponentType] = [], properties: [PropertyType] = []) {
        self.components = components
        self.properties = properties
    }
    
    /**
     Will setup the assembler from the configuration by loading properties and assemblies from the properties
     into the assembler.
     
     - parameter assembler: built assembler
     - parameter flavor:    the flavor to bootstrap the container with
     
     - throws: Swinject.PropertyLoaderError
     */
    public func setup(assembler: Assembler, with flavor: Flavor? = nil) throws {
        for property in properties.filter({ $0.flavor == nil || $0.flavor == flavor }) {
            try assembler.applyPropertyLoader(property.propertyLoader)
            //print("Loaded properties=\(property), for flavor=\(String(describing: flavor))")
        }
        
        // load assemblies from components
        let assemblies = components.map { $0.assemblyForFlavor(flavor) }
        assembler.apply(assemblies: assemblies)
        //print("Applied assemblies=\(assemblies), for flavor=\(String(describing: flavor))")
    }
}
