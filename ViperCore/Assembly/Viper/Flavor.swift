/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Cobra
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Cobra/blob/master/LICENSE
 *
 **************************************************************************************************/

import Foundation

public struct Flavor: Equatable {
    
    public var value: String
    
    public init(_ value: String) {
        self.value = value
    }
    
    public static let Development = Flavor("develop")
    public static let Production = Flavor("production")
    public static let Stage = Flavor("stage")

    public static let supportedFlavors: [Flavor] = [
        .Development,
        .Production,
        .Stage
    ]
    
    public var jsonProperty: JsonProperty {
        return JsonProperty(name: value, flavor: self)
    }
    
    public static var productionFlavor: Flavor? {
        return supportedFlavors.first(where: { $0 == .Production })
    }
}

public func == (lhs: Flavor, rhs: Flavor) -> Bool {
    return lhs.value == rhs.value
}

public protocol EnvironmentsBuilding {
    /**
     Build the environments that are supported for this application. A top level application might not have all of these
     environments. Only the environments that exist in the application bundle will be registered
     
     - returns: the environments for the application
     */
    func buildEnvironments(flavors: [Flavor]) -> [PropertyType]
}
