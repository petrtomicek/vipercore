/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Cobra
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Cobra/blob/master/LICENSE
 *
 **************************************************************************************************/

import Foundation
import Swinject

/**
 The PropertyType protocol provides a thin wrapper around a Swinject PropertyLoaderType that allows certain
 properties to be loaded based on a Cobra Flavor
 */

public protocol PropertyType {

    /// the Swinject property loader that will be used to load properties from disk
    var propertyLoader: PropertyLoader { get }

    /// the flavor in which the properties should be loaded for. If nil, then the properties will always be loaded
    var flavor: Flavor? { get }
}

/**
 The PlistProperty represents a Plist property file that will be loaded into the application based on
 the flavor. If the flavor is nil, then it will always be loaded into the application
 */
public final class PlistProperty: PropertyType {
    
    public let propertyLoader: PropertyLoader
    public let flavor: Flavor?
    
    /**
     Creates a new Plist property type
     
     - parameter bundle: the bundle where the resource exists
     - parameter name:   the name of the resource in the bundle
     - parameter type:   the type of the property resource
     */
    public init(bundle: Bundle = Bundle.main, name: String = "properties", flavor: Flavor? = nil) {
        self.propertyLoader = PlistPropertyLoader(bundle: bundle, name: name)
        self.flavor = flavor
    }
}

extension PlistProperty: CustomStringConvertible {
    public var description: String {
        return "{propertyLoader=\(propertyLoader), flavor=\(String(describing: flavor))}"
    }
}
