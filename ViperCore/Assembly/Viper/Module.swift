/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Cobra
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Cobra/blob/master/LICENSE
 *
 **************************************************************************************************/

import Swinject

/**
 The ModuleType protocol represents a VIPER module and is responsible for managing its assembly
 */
public protocol ModuleType {

    /// the assembly that is backing this module
    var assembly: Assembly { get }

    /// the key for this module that is based on the assembly type itself so we can lookup the module loaded for a proxy
    var key: String { get }
}

/**
 Represents a module that is backed by an assembly. The assembly that this module is bound by will bootstap the
 DI container with service components that make the feature routable
*/
public final class Module<T>: ModuleType where T: Constructible, T: Assembly {
    
    /// will lazy load the assembly when the module is first visited through the proxy
    public lazy var assembly: Assembly = T()
    
    /// the key for this module that is based on the assembly type itself so we can lookup the module loaded for a proxy
    public let key: String
    
    /**
     Creates a module that wraps the Assembly that bootstraps the module
     
     - parameter key: the key for this module
     */
    public init(key: String = "\(T.self)") {
        self.key = key
    }
}
