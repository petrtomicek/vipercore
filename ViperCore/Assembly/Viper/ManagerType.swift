/**
 The ManagerType is an empty protocol that all managers will extend from. This allows us to register a manager
 1 time and let the app figure out its specific type when a certain event happens.
*/
public protocol ManagerType: AnyObject {

}
