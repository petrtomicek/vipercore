/**
 The ServiceType is an empty protocol that all services will extend from. This allows us to register a service
 1 time and let the app figure out its specific type when a certain event happens.
*/
public protocol ServiceType: AnyObject {

}
