import UIKit
import ViperCore

final class ___VARIABLE_moduleName___View: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
        styleViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews() {

    }

    private func setupConstraints() {

    }

    private func styleViews() {

    }
}
