import UIKit

public class ___VARIABLE_moduleName___CellViewModel: Hashable {

    public init() {
        
    }
}

public final class ___VARIABLE_moduleName___Cell: UICollectionViewCell {

    private var viewModel: ___VARIABLE_moduleName___CellViewModel?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
        styleViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews() {

    }

    private func setupConstraints() {

    }

    private func styleViews() {

    }

    public func setup(_ viewModel: ___VARIABLE_moduleName___CellViewModel) {
        self.viewModel = viewModel
    }
}
