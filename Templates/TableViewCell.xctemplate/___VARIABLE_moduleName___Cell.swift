import UIKit

public class ___VARIABLE_moduleName___CellViewModel {

    public init() {
        
    }
}

// sourcery: TableViewRow = ""
public final class ___VARIABLE_moduleName___Cell: UITableViewCell {

    private var viewModel: ___VARIABLE_moduleName___CellViewModel?

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
        styleViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews() {

    }

    private func setupConstraints() {

    }

    private func styleViews() {
        selectionStyle = .none
    }

    public func setup(_ viewModel: ___VARIABLE_moduleName___CellViewModel) {
        self.viewModel = viewModel
    }
}
