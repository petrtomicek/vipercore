import UIKit
import SnapKit
import ViperCore

public protocol ___VARIABLE_moduleName___ViewType: ViewType, ViewPresentable, AlertViewPresentable {
    func display(_ viewModel: ___VARIABLE_moduleName___ModuleViewModel)
}

public typealias ___VARIABLE_moduleName___ViewControllerType = UIViewController & ___VARIABLE_moduleName___ViewType

public final class ___VARIABLE_moduleName___View: UIViewController, CommonTableViewDataSourceDisplayable {

    public let tableView = UITableView()
    var presenter: ___VARIABLE_moduleName___PresenterViewType!
    var viewModel: ___VARIABLE_moduleName___ModuleViewModel?
    public let dataSource = CommonTableViewDataSource<___VARIABLE_moduleName___RowType>()

    public override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        styleViews()
        setupLocalizations()
        presenter.viewLoaded()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    private func setupViews() {
        view.addSubviews(tableView)
    }

    private func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
    }

    private func styleViews() {
        view.backgroundColor = .appWhite
        tableView.backgroundColor = .appWhite
        tableView.separatorStyle = .none
    }
    
    private func setupLocalizations() {
        
    }
}

extension ___VARIABLE_moduleName___View: ___VARIABLE_moduleName___ViewType {

    public func display(_ viewModel: ___VARIABLE_moduleName___ModuleViewModel) {
        setupCommonTableViewDataSource(for: viewModel, and: presenter)
    }
}
