import Swinject
import Foundation
import ViperCore

public final class ___VARIABLE_moduleName___ModuleAssembly: ModuleAssemblyType {

    public required init() {}

    public func assemble(container: Container) {
        registerView(in: container)
        registerPresenter(in: container)
        registerInteractor(in: container)
        registerModule(in: container)
    }

    public func loaded(resolver: Resolver) {

    }

    public func registerView(in container: Container) {
        container.register(___VARIABLE_moduleName___ViewControllerType.self) { _ in
            ___VARIABLE_moduleName___View()
        }
    }

    static func configure(view: ___VARIABLE_moduleName___ViewControllerType, presenter: ___VARIABLE_moduleName___PresenterViewType) {
        let v = view as! ___VARIABLE_moduleName___View
        v.presenter = presenter
    }

    public func registerPresenter(in container: Container) {
        container.register(___VARIABLE_moduleName___PresenterType.self) { (resolver, view: ___VARIABLE_moduleName___ViewControllerType) in
            ___VARIABLE_moduleName___Presenter(
                view: view
            )
        }
    }

    static func configure(presenter: ___VARIABLE_moduleName___PresenterType, interactor: ___VARIABLE_moduleName___InteractorType) {
        let p = presenter as! ___VARIABLE_moduleName___Presenter
        p.interactor = interactor
    }

    public func registerInteractor(in container: Container) {
        container.register(___VARIABLE_moduleName:___InteractorModuleType.self) {
            (
                _,
                presenter: ___VARIABLE_moduleName___PresenterType
            ) in
            ___VARIABLE_moduleName___Interactor(
                presenter: presenter
            )
        }
    }

    public func registerModule(in container: Container) {
        container.register(___VARIABLE_moduleName___ModuleType.self) { resolver in
            let view = resolver.resolve(___VARIABLE_moduleName___ViewControllerType.self)!
            let presenter = resolver.resolve(___VARIABLE_moduleName___PresenterType.self, argument: view)!
            ___VARIABLE_moduleName___ModuleAssembly.configure(view: view, presenter: presenter)
            let interactor = resolver.resolve(___VARIABLE_moduleName___InteractorModuleType.self, argument: presenter)!
            ___VARIABLE_moduleName___ModuleAssembly.configure(presenter: presenter, interactor: interactor)
            return ___VARIABLE_moduleName___Module(
                view: view,
                presenter: presenter,
                interactor: interactor
            )
        }
    }
}
