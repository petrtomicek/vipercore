import Foundation
import ViperCore

public protocol ___VARIABLE_moduleName___InteractorType: InteractorType {
    var initialData: ___VARIABLE_moduleName___InitialData! { get set }
}

public typealias ___VARIABLE_moduleName___InteractorModuleType = ___VARIABLE_moduleName___InteractorType & ___VARIABLE_moduleName___ModuleModel

public final class ___VARIABLE_moduleName___Interactor: ___VARIABLE_moduleName___ModuleModel {

    private(set) weak var presenter: ___VARIABLE_moduleName___PresenterInteractorType?

    public init(presenter: ___VARIABLE_moduleName___PresenterInteractorType) {
        self.presenter = presenter
    }
    
    public var initialData: ___VARIABLE_moduleName___InitialData!
}

extension ___VARIABLE_moduleName___Interactor: ___VARIABLE_moduleName___InteractorType {

}
