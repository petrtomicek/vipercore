import UIKit
import ViperCore

public protocol ___VARIABLE_moduleName___ModuleType: ModuleBaseType {

    var viewController: UIViewController { get }
    var model: ___VARIABLE_moduleName___ModuleModel { get }
    var coordinator: ___VARIABLE_moduleName___ModuleDelegate? { get set }
}

public protocol ___VARIABLE_moduleName___ModuleModel: ModuleModelType {
    var initialData: ___VARIABLE_moduleName___InitialData! { get set }
}

public protocol ___VARIABLE_moduleName___ModuleDelegate: ModuleDelegateType {
    
}

public protocol ___VARIABLE_moduleName___Delegating: ModuleDelegating {

    var coordinator: ___VARIABLE_moduleName___ModuleDelegate? { get set }
}

public final class ___VARIABLE_moduleName___Module: ___VARIABLE_moduleName___ModuleType {

    public var viewController: UIViewController {
        return view
    }

    public var model: ___VARIABLE_moduleName___ModuleModel {
        return interactor
    }

    public var coordinator: ___VARIABLE_moduleName___ModuleDelegate? {
        get {
            return presenter.coordinator
        }
        set {
            presenter.coordinator = newValue
        }
    }

    let view: ___VARIABLE_moduleName___ViewControllerType
    unowned private(set) var presenter: ___VARIABLE_moduleName___PresenterType
    unowned let interactor: ___VARIABLE_moduleName___InteractorModuleType

    public init(
        view: ___VARIABLE_moduleName___ViewControllerType,
        presenter: ___VARIABLE_moduleName___PresenterType,
        interactor: ___VARIABLE_moduleName___InteractorModuleType
    ) {
        self.view = view
        self.presenter = presenter
        self.interactor = interactor
    }
}
