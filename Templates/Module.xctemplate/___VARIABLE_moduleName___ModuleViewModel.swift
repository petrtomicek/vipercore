import Foundation
import ViperCore

public protocol ___VARIABLE_moduleName___ModuleViewModelDelegate: AnyObject {

}

public enum ___VARIABLE_moduleName___RowType: ViewModelRowType {
    case general
}

public final class ___VARIABLE_moduleName___ModuleViewModel: BaseViewModel<___VARIABLE_moduleName___RowType> {

    let section = TableViewSection<___VARIABLE_moduleName___RowType>()
    weak var delegate: ___VARIABLE_moduleName___ModuleViewModelDelegate?

    public init(delegate: ___VARIABLE_moduleName___ModuleViewModelDelegate?) {
        self.delegate = delegate
        super.init()
        setupSections()
    }
    
    private func setupSections() {
        // Add section items here. Adjust sections as needed...
        makeTitleSection()
        sections.append(section)
    }
    
    private func makeTitleSection() {
        
    }
}
