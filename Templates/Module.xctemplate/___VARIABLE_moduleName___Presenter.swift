import Foundation
import ViperCore

public protocol ___VARIABLE_moduleName___PresenterViewType: PresenterViewType, CommonTableViewDataSourcePresentable {
    func viewLoaded()
}

public protocol ___VARIABLE_moduleName___PresenterInteractorType: PresenterInteractorType, PresenterErrorPresentable {
    
}

public typealias ___VARIABLE_moduleName___PresenterType = ___VARIABLE_moduleName___PresenterViewType & ___VARIABLE_moduleName___PresenterInteractorType & ___VARIABLE_moduleName___Delegating

public final class ___VARIABLE_moduleName___Presenter: ___VARIABLE_moduleName___Delegating {

    public var interactor: ___VARIABLE_moduleName___InteractorType!
    public weak var view: ___VARIABLE_moduleName___ViewControllerType?
    public weak var coordinator: ___VARIABLE_moduleName___ModuleDelegate? = nil
    public var viewPresentable: ViewPresentable? { return view }

    init(view: ___VARIABLE_moduleName___ViewType) {
        self.view = view
    }
}

extension ___VARIABLE_moduleName___Presenter: ___VARIABLE_moduleName___PresenterViewType {

    public func viewLoaded() {

    }
    
    private func presentViewModel() {
        let viewModel = ___VARIABLE_moduleName___ModuleViewModel(delegate: self)
        asyncMain {
            self.view?.display(viewModel)
        }
    }
}

extension ___VARIABLE_moduleName___Presenter: ___VARIABLE_moduleName___PresenterInteractorType {

}

extension ___VARIABLE_moduleName___Presenter: ___VARIABLE_moduleName___ModuleViewModelDelegate {
    
}
