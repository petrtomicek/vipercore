import Swinject
import LeeafCore
import LeeafCoreApp

final class ___VARIABLE_moduleName___CoordinatorAssembly: CoordinatorAssemblyType {

    required init() {}

    func assemble(container: Container) {
        registerCoordinator(in: container)
    }

    func loaded(resolver: Resolver) {

    }

    func registerCoordinator(in container: Container) {
        container.register(___VARIABLE_moduleName___CoordinatorType.self) {
            resolver in ___VARIABLE_moduleName___Coordinator()
        }
    }
}
