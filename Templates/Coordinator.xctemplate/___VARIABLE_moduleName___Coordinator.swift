import UIKit
import ViperCore

public protocol ___VARIABLE_moduleName___CoordinatorType: CoordinatorType {

    var navigationController: UINavigationController { get }
    var model: ___VARIABLE_moduleName___CoordinatorModel { get }
    var delegate: ___VARIABLE_moduleName___CoordinatorDelegate? { get set }
}

public protocol ___VARIABLE_moduleName___CoordinatorModel: CoordinatorModelType {

}

public protocol ___VARIABLE_moduleName___CoordinatorDelegate: CoordinatorDelegateType {

}

final class ___VARIABLE_moduleName___Coordinator: ___VARIABLE_moduleName___CoordinatorType, ___VARIABLE_moduleName___CoordinatorModel {

    var model: ___VARIABLE_moduleName___CoordinatorModel { return self }
    weak var delegate: ___VARIABLE_moduleName___CoordinatorDelegate?

    var navigationController: UINavigationController = UINavigationController()
    var currentCoordinator: CoordinatorType?

    func start() {

    }
}

extension ___VARIABLE_moduleName___Coordinator: CoordinatorDelegateType {

    func display(viewController: UIViewController, source: CoordinatorType) {
        delegate?.display(viewController: viewController, source: self)
    }
    
    func push(_ viewController: UIViewController) {
        viewController.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(viewController, animated: true)
    }
}
