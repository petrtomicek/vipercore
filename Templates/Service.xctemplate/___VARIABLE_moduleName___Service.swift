import PromiseKit
import ViperCore

public protocol ___VARIABLE_moduleName___ServiceType {
    func getData() -> Promise<Void>
}

final class ___VARIABLE_moduleName___Service: ___VARIABLE_moduleName___ServiceType {
    let networkingService: NetworkingServiceType
    
    init(networkingService: NetworkingServiceType) {
        self.networkingService = networkingService
    }
    
    func getData() -> Promise<Void> {
        return Promise { resolver in
            resolver.fulfill(())
        }
    }
}
