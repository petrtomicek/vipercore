import PromiseKit
import ViperCore

public protocol ___VARIABLE_moduleName___ManagerType {
    func getData() -> Promise<Void>
}

final class ___VARIABLE_moduleName___Manager: ___VARIABLE_moduleName___ManagerType {
    
    let ___VARIABLE_serviceName___Service: ___VARIABLE_moduleName___ServiceType
    
    init(___VARIABLE_serviceName___Service: ___VARIABLE_moduleName___ServiceType) {
        self.___VARIABLE_serviceName___Service = ___VARIABLE_serviceName___Service
    }
    
    func getData() -> Promise<Void> {
        return ___VARIABLE_serviceName___Service.getData()
    }
}
