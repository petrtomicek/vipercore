import Foundation
import RealmSwift

final class ___VARIABLE_moduleName___Entity: Object, IdentifierType, Decodable {
    
    @Persisted(primaryKey: true) var id: Identifier = ""
    @Persisted var note: String?

    private enum CodingKeys: String, CodingKey {
        case id, note
    }
    
    required override init() { }

    init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            id = try container.decode(String.self, forKey: .id)
            note = try container.decodeIfPresent(String.self, forKey: .note)
        } catch {
            throw ApiError.parsing(message: error.localizedDescription)
        }
    }
}
