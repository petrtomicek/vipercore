Pod::Spec.new do |s|
  s.name                      = 'ViperCore'
  s.module_name               = 'ViperCore'
  s.version                   = '1.0.0'
  s.summary                   = 'Viper Apps core library written in Swift'
  s.homepage                  = 'http://www.atrema.cz'
  s.license                   = 'Proprietary'
  s.author                    = { 'ATREMA' => 'tomicek@atrema.cz' }
  s.platform                  = :ios, '14.0'
  s.ios.deployment_target     = '14.0'
  s.swift_version             = '5.0'
  s.requires_arc              = true
  s.source                    = { :git => 'git@gitlab.com:petrtomicek/vipercore.git', :tag => s.version }
  s.source_files              = ['ViperCore/**/*.{h,swift,m}']
  s.resource                  = []
  s.pod_target_xcconfig      = { 'SWIFT_INSTALL_OBJC_HEADER' => 'NO' }


  # external

  s.dependency 'Swinject', '~> 2.8'
  s.dependency 'DateToolsSwift'

end
